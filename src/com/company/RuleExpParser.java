// Generated from /home/Praca/IdeaProjects/antl4r_test/RuleExp.g4 by ANTLR 4.7
package com.company;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class RuleExpParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__0=1, T__1=2, T__2=3, T__3=4, T__4=5, T__5=6, T__6=7, T__7=8, T__8=9, 
		T__9=10, Assign=11, SEP=12, DECL_SEP=13, LP=14, RP=15, TRUE=16, FALSE=17, 
		IF=18, THEN=19, ELSE=20, ENDIF=21, AND=22, OR=23, NOT=24, Identifier=25, 
		Number=26, Date=27, String=28, QUO=29, WS=30;
	public static final int
		RULE_general_expression = 0, RULE_variable_declarations_list = 1, RULE_variable_declaration = 2, 
		RULE_rule_expression = 3, RULE_conditional_rule = 4, RULE_rule_binary_operator = 5, 
		RULE_rule_unary_operator = 6, RULE_logical_expression = 7, RULE_logical_binary_operator = 8, 
		RULE_logical_unary_operator = 9, RULE_arithmetic_expression = 10, RULE_arithmetic_binary_operator = 11, 
		RULE_arithmetic_unary_operator = 12, RULE_arithmetic_value = 13, RULE_function_invocation = 14, 
		RULE_parameter_list = 15, RULE_empty_parameter_list = 16, RULE_parameter = 17, 
		RULE_variable = 18, RULE_constant_value = 19, RULE_bool = 20;
	public static final String[] ruleNames = {
		"general_expression", "variable_declarations_list", "variable_declaration", 
		"rule_expression", "conditional_rule", "rule_binary_operator", "rule_unary_operator", 
		"logical_expression", "logical_binary_operator", "logical_unary_operator", 
		"arithmetic_expression", "arithmetic_binary_operator", "arithmetic_unary_operator", 
		"arithmetic_value", "function_invocation", "parameter_list", "empty_parameter_list", 
		"parameter", "variable", "constant_value", "bool"
	};

	private static final String[] _LITERAL_NAMES = {
		null, "'<'", "'>'", "'='", "'>='", "'<='", "'!='", "'+'", "'-'", "'*'", 
		"'/'", "':='", "','", "';'", "'('", "')'", null, null, null, null, null, 
		null, null, null, null, null, null, null, null, "'\"'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, null, null, null, null, null, null, null, null, null, null, "Assign", 
		"SEP", "DECL_SEP", "LP", "RP", "TRUE", "FALSE", "IF", "THEN", "ELSE", 
		"ENDIF", "AND", "OR", "NOT", "Identifier", "Number", "Date", "String", 
		"QUO", "WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "RuleExp.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public RuleExpParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class General_expressionContext extends ParserRuleContext {
		public Boolean value;
		public Rule_expressionContext r1;
		public Rule_expressionContext r2;
		public Variable_declarations_listContext variable_declarations_list() {
			return getRuleContext(Variable_declarations_listContext.class,0);
		}
		public TerminalNode EOF() { return getToken(RuleExpParser.EOF, 0); }
		public Rule_expressionContext rule_expression() {
			return getRuleContext(Rule_expressionContext.class,0);
		}
		public General_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_general_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterGeneral_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitGeneral_expression(this);
		}
	}

	public final General_expressionContext general_expression() throws RecognitionException {
		General_expressionContext _localctx = new General_expressionContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_general_expression);
		try {
			setState(51);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(42);
				variable_declarations_list();
				setState(43);
				((General_expressionContext)_localctx).r1 = rule_expression();
				setState(44);
				match(EOF);
				 ((General_expressionContext)_localctx).value =  ((General_expressionContext)_localctx).r1.value; 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(47);
				((General_expressionContext)_localctx).r2 = rule_expression();
				setState(48);
				match(EOF);
				 ((General_expressionContext)_localctx).value =  ((General_expressionContext)_localctx).r2.value; 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_declarations_listContext extends ParserRuleContext {
		public Variable_declarationContext variable_declaration() {
			return getRuleContext(Variable_declarationContext.class,0);
		}
		public TerminalNode DECL_SEP() { return getToken(RuleExpParser.DECL_SEP, 0); }
		public Variable_declarations_listContext variable_declarations_list() {
			return getRuleContext(Variable_declarations_listContext.class,0);
		}
		public Variable_declarations_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_declarations_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterVariable_declarations_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitVariable_declarations_list(this);
		}
	}

	public final Variable_declarations_listContext variable_declarations_list() throws RecognitionException {
		Variable_declarations_listContext _localctx = new Variable_declarations_listContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_variable_declarations_list);
		try {
			setState(60);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(53);
				variable_declaration();
				setState(54);
				match(DECL_SEP);
				setState(55);
				variable_declarations_list();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(57);
				variable_declaration();
				setState(58);
				match(DECL_SEP);
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Variable_declarationContext extends ParserRuleContext {
		public Token i1;
		public Arithmetic_expressionContext a;
		public Token i2;
		public Rule_expressionContext r;
		public TerminalNode Assign() { return getToken(RuleExpParser.Assign, 0); }
		public TerminalNode Identifier() { return getToken(RuleExpParser.Identifier, 0); }
		public Arithmetic_expressionContext arithmetic_expression() {
			return getRuleContext(Arithmetic_expressionContext.class,0);
		}
		public Rule_expressionContext rule_expression() {
			return getRuleContext(Rule_expressionContext.class,0);
		}
		public Variable_declarationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable_declaration; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterVariable_declaration(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitVariable_declaration(this);
		}
	}

	public final Variable_declarationContext variable_declaration() throws RecognitionException {
		Variable_declarationContext _localctx = new Variable_declarationContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_variable_declaration);
		try {
			setState(72);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(62);
				((Variable_declarationContext)_localctx).i1 = match(Identifier);
				setState(63);
				match(Assign);
				setState(64);
				((Variable_declarationContext)_localctx).a = arithmetic_expression();
				 EnvironmentContext.setVariable((((Variable_declarationContext)_localctx).i1!=null?((Variable_declarationContext)_localctx).i1.getText():null), ((Variable_declarationContext)_localctx).a.value); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(67);
				((Variable_declarationContext)_localctx).i2 = match(Identifier);
				setState(68);
				match(Assign);
				setState(69);
				((Variable_declarationContext)_localctx).r = rule_expression();
				 EnvironmentContext.setVariable((((Variable_declarationContext)_localctx).i2!=null?((Variable_declarationContext)_localctx).i2.getText():null), (Comparable) ((Variable_declarationContext)_localctx).r.value); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rule_expressionContext extends ParserRuleContext {
		public Boolean value;
		public Rule_expressionContext l1;
		public Rule_binary_operatorContext rb;
		public Rule_expressionContext rr;
		public Logical_expressionContext le1;
		public Rule_expressionContext re1;
		public Conditional_ruleContext cr;
		public Rule_unary_operatorContext ru;
		public Rule_expressionContext re5;
		public TerminalNode LP() { return getToken(RuleExpParser.LP, 0); }
		public TerminalNode RP() { return getToken(RuleExpParser.RP, 0); }
		public List<Rule_expressionContext> rule_expression() {
			return getRuleContexts(Rule_expressionContext.class);
		}
		public Rule_expressionContext rule_expression(int i) {
			return getRuleContext(Rule_expressionContext.class,i);
		}
		public Rule_binary_operatorContext rule_binary_operator() {
			return getRuleContext(Rule_binary_operatorContext.class,0);
		}
		public Logical_expressionContext logical_expression() {
			return getRuleContext(Logical_expressionContext.class,0);
		}
		public Conditional_ruleContext conditional_rule() {
			return getRuleContext(Conditional_ruleContext.class,0);
		}
		public Rule_unary_operatorContext rule_unary_operator() {
			return getRuleContext(Rule_unary_operatorContext.class,0);
		}
		public Rule_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rule_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterRule_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitRule_expression(this);
		}
	}

	public final Rule_expressionContext rule_expression() throws RecognitionException {
		Rule_expressionContext _localctx = new Rule_expressionContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_rule_expression);
		try {
			setState(99);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,5,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(74);
				match(LP);
				setState(75);
				((Rule_expressionContext)_localctx).l1 = rule_expression();
				setState(76);
				match(RP);
				setState(82);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case AND:
				case OR:
					{
					setState(77);
					((Rule_expressionContext)_localctx).rb = rule_binary_operator();
					setState(78);
					((Rule_expressionContext)_localctx).rr = rule_expression();
					 ((Rule_expressionContext)_localctx).value =  (Boolean) ((Rule_expressionContext)_localctx).rb.value.doAction(((Rule_expressionContext)_localctx).l1.value, ((Rule_expressionContext)_localctx).rr.value); 
					}
					break;
				case EOF:
				case DECL_SEP:
				case RP:
				case THEN:
				case ELSE:
					{
					 ((Rule_expressionContext)_localctx).value =  ((Rule_expressionContext)_localctx).l1.value; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(84);
				((Rule_expressionContext)_localctx).le1 = logical_expression();
				setState(90);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case AND:
				case OR:
					{
					setState(85);
					((Rule_expressionContext)_localctx).rb = rule_binary_operator();
					setState(86);
					((Rule_expressionContext)_localctx).re1 = rule_expression();
					 ((Rule_expressionContext)_localctx).value =  (Boolean) ((Rule_expressionContext)_localctx).rb.value.doAction(((Rule_expressionContext)_localctx).le1.value, ((Rule_expressionContext)_localctx).re1.value); 
					}
					break;
				case EOF:
				case DECL_SEP:
				case RP:
				case THEN:
				case ELSE:
					{
					 ((Rule_expressionContext)_localctx).value =  ((Rule_expressionContext)_localctx).le1.value; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(92);
				((Rule_expressionContext)_localctx).cr = conditional_rule();
				 ((Rule_expressionContext)_localctx).value =  ((Rule_expressionContext)_localctx).cr.value; 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(95);
				((Rule_expressionContext)_localctx).ru = rule_unary_operator();
				setState(96);
				((Rule_expressionContext)_localctx).re5 = rule_expression();
				 ((Rule_expressionContext)_localctx).value =  (Boolean) ((Rule_expressionContext)_localctx).ru.value.doAction(((Rule_expressionContext)_localctx).re5.value); 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Conditional_ruleContext extends ParserRuleContext {
		public Boolean value;
		public Rule_expressionContext cond;
		public Rule_expressionContext what_then;
		public Rule_expressionContext what_else;
		public TerminalNode IF() { return getToken(RuleExpParser.IF, 0); }
		public TerminalNode THEN() { return getToken(RuleExpParser.THEN, 0); }
		public List<Rule_expressionContext> rule_expression() {
			return getRuleContexts(Rule_expressionContext.class);
		}
		public Rule_expressionContext rule_expression(int i) {
			return getRuleContext(Rule_expressionContext.class,i);
		}
		public TerminalNode ELSE() { return getToken(RuleExpParser.ELSE, 0); }
		public Conditional_ruleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_conditional_rule; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterConditional_rule(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitConditional_rule(this);
		}
	}

	public final Conditional_ruleContext conditional_rule() throws RecognitionException {
		Conditional_ruleContext _localctx = new Conditional_ruleContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_conditional_rule);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(101);
			match(IF);
			setState(102);
			((Conditional_ruleContext)_localctx).cond = rule_expression();
			setState(103);
			match(THEN);
			setState(104);
			((Conditional_ruleContext)_localctx).what_then = rule_expression();
			setState(110);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
			case 1:
				{
				setState(105);
				match(ELSE);
				setState(106);
				((Conditional_ruleContext)_localctx).what_else = rule_expression();
				 ((Conditional_ruleContext)_localctx).value =  (((Conditional_ruleContext)_localctx).cond.value && ((Conditional_ruleContext)_localctx).what_then.value) || (!((Conditional_ruleContext)_localctx).cond.value && ((Conditional_ruleContext)_localctx).what_else.value); 
				}
				break;
			case 2:
				{
				 ((Conditional_ruleContext)_localctx).value =  (((Conditional_ruleContext)_localctx).cond.value && ((Conditional_ruleContext)_localctx).what_then.value) || !((Conditional_ruleContext)_localctx).cond.value; 
				}
				break;
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rule_binary_operatorContext extends ParserRuleContext {
		public NArgAction value;
		public TerminalNode AND() { return getToken(RuleExpParser.AND, 0); }
		public TerminalNode OR() { return getToken(RuleExpParser.OR, 0); }
		public Rule_binary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rule_binary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterRule_binary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitRule_binary_operator(this);
		}
	}

	public final Rule_binary_operatorContext rule_binary_operator() throws RecognitionException {
		Rule_binary_operatorContext _localctx = new Rule_binary_operatorContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_rule_binary_operator);
		try {
			setState(116);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case AND:
				enterOuterAlt(_localctx, 1);
				{
				setState(112);
				match(AND);
				 ((Rule_binary_operatorContext)_localctx).value =  params -> (Boolean) params[0] && (Boolean) params[1]; 
				}
				break;
			case OR:
				enterOuterAlt(_localctx, 2);
				{
				setState(114);
				match(OR);
				 ((Rule_binary_operatorContext)_localctx).value =  params -> (Boolean) params[0] || (Boolean) params[1]; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Rule_unary_operatorContext extends ParserRuleContext {
		public NArgAction value;
		public TerminalNode NOT() { return getToken(RuleExpParser.NOT, 0); }
		public Rule_unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_rule_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterRule_unary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitRule_unary_operator(this);
		}
	}

	public final Rule_unary_operatorContext rule_unary_operator() throws RecognitionException {
		Rule_unary_operatorContext _localctx = new Rule_unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_rule_unary_operator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(118);
			match(NOT);
			 ((Rule_unary_operatorContext)_localctx).value =  params -> !((Boolean) params[0]); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_expressionContext extends ParserRuleContext {
		public Boolean value;
		public Logical_unary_operatorContext lu;
		public Logical_expressionContext a;
		public Arithmetic_expressionContext a1;
		public Logical_binary_operatorContext c;
		public Arithmetic_expressionContext a2;
		public BoolContext b;
		public Logical_unary_operatorContext logical_unary_operator() {
			return getRuleContext(Logical_unary_operatorContext.class,0);
		}
		public Logical_expressionContext logical_expression() {
			return getRuleContext(Logical_expressionContext.class,0);
		}
		public List<Arithmetic_expressionContext> arithmetic_expression() {
			return getRuleContexts(Arithmetic_expressionContext.class);
		}
		public Arithmetic_expressionContext arithmetic_expression(int i) {
			return getRuleContext(Arithmetic_expressionContext.class,i);
		}
		public Logical_binary_operatorContext logical_binary_operator() {
			return getRuleContext(Logical_binary_operatorContext.class,0);
		}
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public Logical_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterLogical_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitLogical_expression(this);
		}
	}

	public final Logical_expressionContext logical_expression() throws RecognitionException {
		Logical_expressionContext _localctx = new Logical_expressionContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_logical_expression);
		try {
			setState(133);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,8,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(121);
				((Logical_expressionContext)_localctx).lu = logical_unary_operator();
				setState(122);
				((Logical_expressionContext)_localctx).a = logical_expression();
				 ((Logical_expressionContext)_localctx).value =  (Boolean) ((Logical_expressionContext)_localctx).lu.value.doAction(((Logical_expressionContext)_localctx).a.value); 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(125);
				((Logical_expressionContext)_localctx).a1 = arithmetic_expression();
				setState(126);
				((Logical_expressionContext)_localctx).c = logical_binary_operator();
				setState(127);
				((Logical_expressionContext)_localctx).a2 = arithmetic_expression();
				 ((Logical_expressionContext)_localctx).value =  (Boolean) ((Logical_expressionContext)_localctx).c.value.doAction(((Logical_expressionContext)_localctx).a1.value, ((Logical_expressionContext)_localctx).a2.value); 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(130);
				((Logical_expressionContext)_localctx).b = bool();
				 ((Logical_expressionContext)_localctx).value =  ((Logical_expressionContext)_localctx).b.value; 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_binary_operatorContext extends ParserRuleContext {
		public NArgAction value;
		public Logical_binary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_binary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterLogical_binary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitLogical_binary_operator(this);
		}
	}

	public final Logical_binary_operatorContext logical_binary_operator() throws RecognitionException {
		Logical_binary_operatorContext _localctx = new Logical_binary_operatorContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_logical_binary_operator);
		try {
			setState(147);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__0:
				enterOuterAlt(_localctx, 1);
				{
				setState(135);
				match(T__0);
				 ((Logical_binary_operatorContext)_localctx).value =  params -> params[0].compareTo(params[1]) < 0; 
				}
				break;
			case T__1:
				enterOuterAlt(_localctx, 2);
				{
				setState(137);
				match(T__1);
				 ((Logical_binary_operatorContext)_localctx).value =  params -> params[0].compareTo(params[1]) > 0; 
				}
				break;
			case T__2:
				enterOuterAlt(_localctx, 3);
				{
				setState(139);
				match(T__2);
				 ((Logical_binary_operatorContext)_localctx).value =  params -> params[0].compareTo(params[1]) == 0; 
				}
				break;
			case T__3:
				enterOuterAlt(_localctx, 4);
				{
				setState(141);
				match(T__3);
				 ((Logical_binary_operatorContext)_localctx).value =  params -> params[0].compareTo(params[1]) >= 0; 
				}
				break;
			case T__4:
				enterOuterAlt(_localctx, 5);
				{
				setState(143);
				match(T__4);
				 ((Logical_binary_operatorContext)_localctx).value =  params -> params[0].compareTo(params[1]) <= 0; 
				}
				break;
			case T__5:
				enterOuterAlt(_localctx, 6);
				{
				setState(145);
				match(T__5);
				 ((Logical_binary_operatorContext)_localctx).value =  params -> params[0].compareTo(params[1]) != 0; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Logical_unary_operatorContext extends ParserRuleContext {
		public NArgAction value;
		public TerminalNode NOT() { return getToken(RuleExpParser.NOT, 0); }
		public Logical_unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_logical_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterLogical_unary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitLogical_unary_operator(this);
		}
	}

	public final Logical_unary_operatorContext logical_unary_operator() throws RecognitionException {
		Logical_unary_operatorContext _localctx = new Logical_unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_logical_unary_operator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(149);
			match(NOT);
			 ((Logical_unary_operatorContext)_localctx).value =  params -> !((Boolean) params[0]); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Arithmetic_expressionContext extends ParserRuleContext {
		public Comparable value;
		public Arithmetic_expressionContext ae1;
		public Arithmetic_binary_operatorContext ab;
		public Arithmetic_expressionContext ae2;
		public Arithmetic_unary_operatorContext au;
		public Arithmetic_expressionContext ae;
		public Arithmetic_valueContext av;
		public Function_invocationContext fi;
		public Arithmetic_valueContext arith_val;
		public TerminalNode LP() { return getToken(RuleExpParser.LP, 0); }
		public TerminalNode RP() { return getToken(RuleExpParser.RP, 0); }
		public List<Arithmetic_expressionContext> arithmetic_expression() {
			return getRuleContexts(Arithmetic_expressionContext.class);
		}
		public Arithmetic_expressionContext arithmetic_expression(int i) {
			return getRuleContext(Arithmetic_expressionContext.class,i);
		}
		public Arithmetic_binary_operatorContext arithmetic_binary_operator() {
			return getRuleContext(Arithmetic_binary_operatorContext.class,0);
		}
		public Arithmetic_unary_operatorContext arithmetic_unary_operator() {
			return getRuleContext(Arithmetic_unary_operatorContext.class,0);
		}
		public Arithmetic_valueContext arithmetic_value() {
			return getRuleContext(Arithmetic_valueContext.class,0);
		}
		public Function_invocationContext function_invocation() {
			return getRuleContext(Function_invocationContext.class,0);
		}
		public Arithmetic_expressionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmetic_expression; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterArithmetic_expression(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitArithmetic_expression(this);
		}
	}

	public final Arithmetic_expressionContext arithmetic_expression() throws RecognitionException {
		Arithmetic_expressionContext _localctx = new Arithmetic_expressionContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_arithmetic_expression);
		try {
			setState(177);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,11,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(152);
				match(LP);
				setState(153);
				((Arithmetic_expressionContext)_localctx).ae1 = arithmetic_expression();
				setState(154);
				match(RP);
				setState(160);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
				case T__6:
				case T__7:
				case T__8:
				case T__9:
					{
					setState(155);
					((Arithmetic_expressionContext)_localctx).ab = arithmetic_binary_operator();
					setState(156);
					((Arithmetic_expressionContext)_localctx).ae2 = arithmetic_expression();
					 ((Arithmetic_expressionContext)_localctx).value =  (Double) ((Arithmetic_expressionContext)_localctx).ab.value.doAction(((Arithmetic_expressionContext)_localctx).ae1.value, ((Arithmetic_expressionContext)_localctx).ae2.value); 
					}
					break;
				case EOF:
				case T__0:
				case T__1:
				case T__2:
				case T__3:
				case T__4:
				case T__5:
				case SEP:
				case DECL_SEP:
				case RP:
				case THEN:
				case ELSE:
				case AND:
				case OR:
					{
					 ((Arithmetic_expressionContext)_localctx).value =  ((Arithmetic_expressionContext)_localctx).ae1.value; 
					}
					break;
				default:
					throw new NoViableAltException(this);
				}
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(162);
				((Arithmetic_expressionContext)_localctx).au = arithmetic_unary_operator();
				setState(163);
				((Arithmetic_expressionContext)_localctx).ae = arithmetic_expression();
				 ((Arithmetic_expressionContext)_localctx).value =  (Double) ((Arithmetic_expressionContext)_localctx).au.value.doAction(((Arithmetic_expressionContext)_localctx).ae.value); 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(166);
				((Arithmetic_expressionContext)_localctx).av = arithmetic_value();
				setState(167);
				((Arithmetic_expressionContext)_localctx).ab = arithmetic_binary_operator();
				setState(168);
				((Arithmetic_expressionContext)_localctx).ae = arithmetic_expression();
				 ((Arithmetic_expressionContext)_localctx).value =  (Double) ((Arithmetic_expressionContext)_localctx).ab.value.doAction(((Arithmetic_expressionContext)_localctx).av.value, ((Arithmetic_expressionContext)_localctx).ae.value); 
				}
				break;
			case 4:
				enterOuterAlt(_localctx, 4);
				{
				setState(171);
				((Arithmetic_expressionContext)_localctx).fi = function_invocation();
				 ((Arithmetic_expressionContext)_localctx).value =  ((Arithmetic_expressionContext)_localctx).fi.value; 
				}
				break;
			case 5:
				enterOuterAlt(_localctx, 5);
				{
				setState(174);
				((Arithmetic_expressionContext)_localctx).arith_val = arithmetic_value();
				 ((Arithmetic_expressionContext)_localctx).value =  ((Arithmetic_expressionContext)_localctx).arith_val.value; 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Arithmetic_binary_operatorContext extends ParserRuleContext {
		public NArgAction value;
		public Arithmetic_binary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmetic_binary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterArithmetic_binary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitArithmetic_binary_operator(this);
		}
	}

	public final Arithmetic_binary_operatorContext arithmetic_binary_operator() throws RecognitionException {
		Arithmetic_binary_operatorContext _localctx = new Arithmetic_binary_operatorContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_arithmetic_binary_operator);
		try {
			setState(187);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case T__6:
				enterOuterAlt(_localctx, 1);
				{
				setState(179);
				match(T__6);
				 ((Arithmetic_binary_operatorContext)_localctx).value =  (params -> (Double) params[0] + (Double) params[1]); 
				}
				break;
			case T__7:
				enterOuterAlt(_localctx, 2);
				{
				setState(181);
				match(T__7);
				 ((Arithmetic_binary_operatorContext)_localctx).value =  (params -> (Double) params[0] - (Double) params[1]); 
				}
				break;
			case T__8:
				enterOuterAlt(_localctx, 3);
				{
				setState(183);
				match(T__8);
				 ((Arithmetic_binary_operatorContext)_localctx).value =  (params -> (Double) params[0] * (Double) params[1]); 
				}
				break;
			case T__9:
				enterOuterAlt(_localctx, 4);
				{
				setState(185);
				match(T__9);
				 ((Arithmetic_binary_operatorContext)_localctx).value =  (params -> (Double) params[0] / (Double) params[1]); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Arithmetic_unary_operatorContext extends ParserRuleContext {
		public NArgAction value;
		public Arithmetic_unary_operatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmetic_unary_operator; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterArithmetic_unary_operator(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitArithmetic_unary_operator(this);
		}
	}

	public final Arithmetic_unary_operatorContext arithmetic_unary_operator() throws RecognitionException {
		Arithmetic_unary_operatorContext _localctx = new Arithmetic_unary_operatorContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_arithmetic_unary_operator);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(189);
			match(T__7);
			 ((Arithmetic_unary_operatorContext)_localctx).value =  (params -> ( (-1.0) * (Double) params[0]) ); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Arithmetic_valueContext extends ParserRuleContext {
		public Comparable value;
		public Constant_valueContext c;
		public VariableContext v;
		public Constant_valueContext constant_value() {
			return getRuleContext(Constant_valueContext.class,0);
		}
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Arithmetic_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_arithmetic_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterArithmetic_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitArithmetic_value(this);
		}
	}

	public final Arithmetic_valueContext arithmetic_value() throws RecognitionException {
		Arithmetic_valueContext _localctx = new Arithmetic_valueContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_arithmetic_value);
		try {
			setState(198);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUE:
			case FALSE:
			case Number:
			case Date:
			case String:
				enterOuterAlt(_localctx, 1);
				{
				setState(192);
				((Arithmetic_valueContext)_localctx).c = constant_value();
				 ((Arithmetic_valueContext)_localctx).value =  ((Arithmetic_valueContext)_localctx).c.value; 
				}
				break;
			case Identifier:
				enterOuterAlt(_localctx, 2);
				{
				setState(195);
				((Arithmetic_valueContext)_localctx).v = variable();
				 ((Arithmetic_valueContext)_localctx).value =  ((Arithmetic_valueContext)_localctx).v.value; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Function_invocationContext extends ParserRuleContext {
		public Comparable value;
		public Token i;
		public Parameter_listContext p;
		public TerminalNode LP() { return getToken(RuleExpParser.LP, 0); }
		public TerminalNode RP() { return getToken(RuleExpParser.RP, 0); }
		public TerminalNode Identifier() { return getToken(RuleExpParser.Identifier, 0); }
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public Function_invocationContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_function_invocation; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterFunction_invocation(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitFunction_invocation(this);
		}
	}

	public final Function_invocationContext function_invocation() throws RecognitionException {
		Function_invocationContext _localctx = new Function_invocationContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_function_invocation);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(200);
			((Function_invocationContext)_localctx).i = match(Identifier);
			setState(201);
			match(LP);
			setState(202);
			((Function_invocationContext)_localctx).p = parameter_list();
			setState(203);
			match(RP);

			            Comparable[] params = new Comparable[((Function_invocationContext)_localctx).p.value.size()];
			            for (int i = 0; i < ((Function_invocationContext)_localctx).p.value.size(); i++) params[i] = (Comparable) ((Function_invocationContext)_localctx).p.value.get(i);
			            ((Function_invocationContext)_localctx).value =  EnvironmentContext.getFunction((((Function_invocationContext)_localctx).i!=null?((Function_invocationContext)_localctx).i.getText():null)).doAction(params);
			        
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Parameter_listContext extends ParserRuleContext {
		public java.util.LinkedList<Comparable> value;
		public ParameterContext p;
		public Parameter_listContext pl;
		public Empty_parameter_listContext epl;
		public ParameterContext parameter() {
			return getRuleContext(ParameterContext.class,0);
		}
		public TerminalNode SEP() { return getToken(RuleExpParser.SEP, 0); }
		public Parameter_listContext parameter_list() {
			return getRuleContext(Parameter_listContext.class,0);
		}
		public Empty_parameter_listContext empty_parameter_list() {
			return getRuleContext(Empty_parameter_listContext.class,0);
		}
		public Parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterParameter_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitParameter_list(this);
		}
	}

	public final Parameter_listContext parameter_list() throws RecognitionException {
		Parameter_listContext _localctx = new Parameter_listContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_parameter_list);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(206);
			((Parameter_listContext)_localctx).p = parameter();
			setState(214);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case SEP:
				{
				setState(207);
				match(SEP);
				setState(208);
				((Parameter_listContext)_localctx).pl = parameter_list();
				 ((Parameter_listContext)_localctx).pl.value.add(((Parameter_listContext)_localctx).p.value); ((Parameter_listContext)_localctx).value =  ((Parameter_listContext)_localctx).pl.value; 
				}
				break;
			case RP:
				{
				setState(211);
				((Parameter_listContext)_localctx).epl = empty_parameter_list();
				 ((Parameter_listContext)_localctx).epl.value.add(((Parameter_listContext)_localctx).p.value); ((Parameter_listContext)_localctx).value =  ((Parameter_listContext)_localctx).epl.value; 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Empty_parameter_listContext extends ParserRuleContext {
		public java.util.LinkedList<Comparable> value;
		public Empty_parameter_listContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_empty_parameter_list; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterEmpty_parameter_list(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitEmpty_parameter_list(this);
		}
	}

	public final Empty_parameter_listContext empty_parameter_list() throws RecognitionException {
		Empty_parameter_listContext _localctx = new Empty_parameter_listContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_empty_parameter_list);
		try {
			enterOuterAlt(_localctx, 1);
			{
			 ((Empty_parameter_listContext)_localctx).value =  new java.util.LinkedList<Comparable>(); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ParameterContext extends ParserRuleContext {
		public Comparable value;
		public VariableContext v;
		public Arithmetic_expressionContext ae;
		public Logical_expressionContext le;
		public VariableContext variable() {
			return getRuleContext(VariableContext.class,0);
		}
		public Arithmetic_expressionContext arithmetic_expression() {
			return getRuleContext(Arithmetic_expressionContext.class,0);
		}
		public Logical_expressionContext logical_expression() {
			return getRuleContext(Logical_expressionContext.class,0);
		}
		public ParameterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_parameter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterParameter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitParameter(this);
		}
	}

	public final ParameterContext parameter() throws RecognitionException {
		ParameterContext _localctx = new ParameterContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_parameter);
		try {
			setState(227);
			_errHandler.sync(this);
			switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(218);
				((ParameterContext)_localctx).v = variable();
				 ((ParameterContext)_localctx).value =  ((ParameterContext)_localctx).v.value; 
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(221);
				((ParameterContext)_localctx).ae = arithmetic_expression();
				 ((ParameterContext)_localctx).value =  ((ParameterContext)_localctx).ae.value; 
				}
				break;
			case 3:
				enterOuterAlt(_localctx, 3);
				{
				setState(224);
				((ParameterContext)_localctx).le = logical_expression();
				 ((ParameterContext)_localctx).value =  (Comparable) ((ParameterContext)_localctx).le.value; 
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class VariableContext extends ParserRuleContext {
		public Comparable value;
		public Token i;
		public TerminalNode Identifier() { return getToken(RuleExpParser.Identifier, 0); }
		public VariableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_variable; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterVariable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitVariable(this);
		}
	}

	public final VariableContext variable() throws RecognitionException {
		VariableContext _localctx = new VariableContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_variable);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(229);
			((VariableContext)_localctx).i = match(Identifier);
			 ((VariableContext)_localctx).value =  EnvironmentContext.getVariable((((VariableContext)_localctx).i!=null?((VariableContext)_localctx).i.getText():null)); 
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class Constant_valueContext extends ParserRuleContext {
		public Comparable value;
		public Token n;
		public BoolContext b;
		public Token s;
		public Token d;
		public TerminalNode Number() { return getToken(RuleExpParser.Number, 0); }
		public BoolContext bool() {
			return getRuleContext(BoolContext.class,0);
		}
		public TerminalNode String() { return getToken(RuleExpParser.String, 0); }
		public TerminalNode Date() { return getToken(RuleExpParser.Date, 0); }
		public Constant_valueContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_constant_value; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterConstant_value(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitConstant_value(this);
		}
	}

	public final Constant_valueContext constant_value() throws RecognitionException {
		Constant_valueContext _localctx = new Constant_valueContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_constant_value);
		try {
			setState(241);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case Number:
				enterOuterAlt(_localctx, 1);
				{
				setState(232);
				((Constant_valueContext)_localctx).n = match(Number);
				((Constant_valueContext)_localctx).value =  Double.parseDouble((((Constant_valueContext)_localctx).n!=null?((Constant_valueContext)_localctx).n.getText():null));
				}
				break;
			case TRUE:
			case FALSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(234);
				((Constant_valueContext)_localctx).b = bool();
				 ((Constant_valueContext)_localctx).value =  ((Constant_valueContext)_localctx).b.value; 
				}
				break;
			case String:
				enterOuterAlt(_localctx, 3);
				{
				setState(237);
				((Constant_valueContext)_localctx).s = match(String);
				((Constant_valueContext)_localctx).value =  (((Constant_valueContext)_localctx).s!=null?((Constant_valueContext)_localctx).s.getText():null);
				}
				break;
			case Date:
				enterOuterAlt(_localctx, 4);
				{
				setState(239);
				((Constant_valueContext)_localctx).d = match(Date);
				((Constant_valueContext)_localctx).value =  (((Constant_valueContext)_localctx).d!=null?((Constant_valueContext)_localctx).d.getText():null);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BoolContext extends ParserRuleContext {
		public Boolean value;
		public TerminalNode TRUE() { return getToken(RuleExpParser.TRUE, 0); }
		public TerminalNode FALSE() { return getToken(RuleExpParser.FALSE, 0); }
		public BoolContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bool; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).enterBool(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof RuleExpListener ) ((RuleExpListener)listener).exitBool(this);
		}
	}

	public final BoolContext bool() throws RecognitionException {
		BoolContext _localctx = new BoolContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_bool);
		try {
			setState(247);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case TRUE:
				enterOuterAlt(_localctx, 1);
				{
				setState(243);
				match(TRUE);
				 ((BoolContext)_localctx).value =  new Boolean(true); 
				}
				break;
			case FALSE:
				enterOuterAlt(_localctx, 2);
				{
				setState(245);
				match(FALSE);
				 ((BoolContext)_localctx).value =  new Boolean(false); 
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3 \u00fc\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t"+
		"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22"+
		"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\3\2\3\2\3\2\3\2\3\2\3\2\3\2\3"+
		"\2\3\2\5\2\66\n\2\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3?\n\3\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\5\4K\n\4\3\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5"+
		"U\n\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5]\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\5\5\5"+
		"f\n\5\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\3\6\5\6q\n\6\3\7\3\7\3\7\3\7\5\7"+
		"w\n\7\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t"+
		"\u0088\n\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\3\n\5\n\u0096\n"+
		"\n\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00a3\n\f\3\f\3"+
		"\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\3\f\5\f\u00b4\n\f\3"+
		"\r\3\r\3\r\3\r\3\r\3\r\3\r\3\r\5\r\u00be\n\r\3\16\3\16\3\16\3\17\3\17"+
		"\3\17\3\17\3\17\3\17\5\17\u00c9\n\17\3\20\3\20\3\20\3\20\3\20\3\20\3\21"+
		"\3\21\3\21\3\21\3\21\3\21\3\21\3\21\5\21\u00d9\n\21\3\22\3\22\3\23\3\23"+
		"\3\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u00e6\n\23\3\24\3\24\3\24\3\25"+
		"\3\25\3\25\3\25\3\25\3\25\3\25\3\25\3\25\5\25\u00f4\n\25\3\26\3\26\3\26"+
		"\3\26\5\26\u00fa\n\26\3\26\2\2\27\2\4\6\b\n\f\16\20\22\24\26\30\32\34"+
		"\36 \"$&(*\2\2\2\u0107\2\65\3\2\2\2\4>\3\2\2\2\6J\3\2\2\2\be\3\2\2\2\n"+
		"g\3\2\2\2\fv\3\2\2\2\16x\3\2\2\2\20\u0087\3\2\2\2\22\u0095\3\2\2\2\24"+
		"\u0097\3\2\2\2\26\u00b3\3\2\2\2\30\u00bd\3\2\2\2\32\u00bf\3\2\2\2\34\u00c8"+
		"\3\2\2\2\36\u00ca\3\2\2\2 \u00d0\3\2\2\2\"\u00da\3\2\2\2$\u00e5\3\2\2"+
		"\2&\u00e7\3\2\2\2(\u00f3\3\2\2\2*\u00f9\3\2\2\2,-\5\4\3\2-.\5\b\5\2./"+
		"\7\2\2\3/\60\b\2\1\2\60\66\3\2\2\2\61\62\5\b\5\2\62\63\7\2\2\3\63\64\b"+
		"\2\1\2\64\66\3\2\2\2\65,\3\2\2\2\65\61\3\2\2\2\66\3\3\2\2\2\678\5\6\4"+
		"\289\7\17\2\29:\5\4\3\2:?\3\2\2\2;<\5\6\4\2<=\7\17\2\2=?\3\2\2\2>\67\3"+
		"\2\2\2>;\3\2\2\2?\5\3\2\2\2@A\7\33\2\2AB\7\r\2\2BC\5\26\f\2CD\b\4\1\2"+
		"DK\3\2\2\2EF\7\33\2\2FG\7\r\2\2GH\5\b\5\2HI\b\4\1\2IK\3\2\2\2J@\3\2\2"+
		"\2JE\3\2\2\2K\7\3\2\2\2LM\7\20\2\2MN\5\b\5\2NT\7\21\2\2OP\5\f\7\2PQ\5"+
		"\b\5\2QR\b\5\1\2RU\3\2\2\2SU\b\5\1\2TO\3\2\2\2TS\3\2\2\2Uf\3\2\2\2V\\"+
		"\5\20\t\2WX\5\f\7\2XY\5\b\5\2YZ\b\5\1\2Z]\3\2\2\2[]\b\5\1\2\\W\3\2\2\2"+
		"\\[\3\2\2\2]f\3\2\2\2^_\5\n\6\2_`\b\5\1\2`f\3\2\2\2ab\5\16\b\2bc\5\b\5"+
		"\2cd\b\5\1\2df\3\2\2\2eL\3\2\2\2eV\3\2\2\2e^\3\2\2\2ea\3\2\2\2f\t\3\2"+
		"\2\2gh\7\24\2\2hi\5\b\5\2ij\7\25\2\2jp\5\b\5\2kl\7\26\2\2lm\5\b\5\2mn"+
		"\b\6\1\2nq\3\2\2\2oq\b\6\1\2pk\3\2\2\2po\3\2\2\2q\13\3\2\2\2rs\7\30\2"+
		"\2sw\b\7\1\2tu\7\31\2\2uw\b\7\1\2vr\3\2\2\2vt\3\2\2\2w\r\3\2\2\2xy\7\32"+
		"\2\2yz\b\b\1\2z\17\3\2\2\2{|\5\24\13\2|}\5\20\t\2}~\b\t\1\2~\u0088\3\2"+
		"\2\2\177\u0080\5\26\f\2\u0080\u0081\5\22\n\2\u0081\u0082\5\26\f\2\u0082"+
		"\u0083\b\t\1\2\u0083\u0088\3\2\2\2\u0084\u0085\5*\26\2\u0085\u0086\b\t"+
		"\1\2\u0086\u0088\3\2\2\2\u0087{\3\2\2\2\u0087\177\3\2\2\2\u0087\u0084"+
		"\3\2\2\2\u0088\21\3\2\2\2\u0089\u008a\7\3\2\2\u008a\u0096\b\n\1\2\u008b"+
		"\u008c\7\4\2\2\u008c\u0096\b\n\1\2\u008d\u008e\7\5\2\2\u008e\u0096\b\n"+
		"\1\2\u008f\u0090\7\6\2\2\u0090\u0096\b\n\1\2\u0091\u0092\7\7\2\2\u0092"+
		"\u0096\b\n\1\2\u0093\u0094\7\b\2\2\u0094\u0096\b\n\1\2\u0095\u0089\3\2"+
		"\2\2\u0095\u008b\3\2\2\2\u0095\u008d\3\2\2\2\u0095\u008f\3\2\2\2\u0095"+
		"\u0091\3\2\2\2\u0095\u0093\3\2\2\2\u0096\23\3\2\2\2\u0097\u0098\7\32\2"+
		"\2\u0098\u0099\b\13\1\2\u0099\25\3\2\2\2\u009a\u009b\7\20\2\2\u009b\u009c"+
		"\5\26\f\2\u009c\u00a2\7\21\2\2\u009d\u009e\5\30\r\2\u009e\u009f\5\26\f"+
		"\2\u009f\u00a0\b\f\1\2\u00a0\u00a3\3\2\2\2\u00a1\u00a3\b\f\1\2\u00a2\u009d"+
		"\3\2\2\2\u00a2\u00a1\3\2\2\2\u00a3\u00b4\3\2\2\2\u00a4\u00a5\5\32\16\2"+
		"\u00a5\u00a6\5\26\f\2\u00a6\u00a7\b\f\1\2\u00a7\u00b4\3\2\2\2\u00a8\u00a9"+
		"\5\34\17\2\u00a9\u00aa\5\30\r\2\u00aa\u00ab\5\26\f\2\u00ab\u00ac\b\f\1"+
		"\2\u00ac\u00b4\3\2\2\2\u00ad\u00ae\5\36\20\2\u00ae\u00af\b\f\1\2\u00af"+
		"\u00b4\3\2\2\2\u00b0\u00b1\5\34\17\2\u00b1\u00b2\b\f\1\2\u00b2\u00b4\3"+
		"\2\2\2\u00b3\u009a\3\2\2\2\u00b3\u00a4\3\2\2\2\u00b3\u00a8\3\2\2\2\u00b3"+
		"\u00ad\3\2\2\2\u00b3\u00b0\3\2\2\2\u00b4\27\3\2\2\2\u00b5\u00b6\7\t\2"+
		"\2\u00b6\u00be\b\r\1\2\u00b7\u00b8\7\n\2\2\u00b8\u00be\b\r\1\2\u00b9\u00ba"+
		"\7\13\2\2\u00ba\u00be\b\r\1\2\u00bb\u00bc\7\f\2\2\u00bc\u00be\b\r\1\2"+
		"\u00bd\u00b5\3\2\2\2\u00bd\u00b7\3\2\2\2\u00bd\u00b9\3\2\2\2\u00bd\u00bb"+
		"\3\2\2\2\u00be\31\3\2\2\2\u00bf\u00c0\7\n\2\2\u00c0\u00c1\b\16\1\2\u00c1"+
		"\33\3\2\2\2\u00c2\u00c3\5(\25\2\u00c3\u00c4\b\17\1\2\u00c4\u00c9\3\2\2"+
		"\2\u00c5\u00c6\5&\24\2\u00c6\u00c7\b\17\1\2\u00c7\u00c9\3\2\2\2\u00c8"+
		"\u00c2\3\2\2\2\u00c8\u00c5\3\2\2\2\u00c9\35\3\2\2\2\u00ca\u00cb\7\33\2"+
		"\2\u00cb\u00cc\7\20\2\2\u00cc\u00cd\5 \21\2\u00cd\u00ce\7\21\2\2\u00ce"+
		"\u00cf\b\20\1\2\u00cf\37\3\2\2\2\u00d0\u00d8\5$\23\2\u00d1\u00d2\7\16"+
		"\2\2\u00d2\u00d3\5 \21\2\u00d3\u00d4\b\21\1\2\u00d4\u00d9\3\2\2\2\u00d5"+
		"\u00d6\5\"\22\2\u00d6\u00d7\b\21\1\2\u00d7\u00d9\3\2\2\2\u00d8\u00d1\3"+
		"\2\2\2\u00d8\u00d5\3\2\2\2\u00d9!\3\2\2\2\u00da\u00db\b\22\1\2\u00db#"+
		"\3\2\2\2\u00dc\u00dd\5&\24\2\u00dd\u00de\b\23\1\2\u00de\u00e6\3\2\2\2"+
		"\u00df\u00e0\5\26\f\2\u00e0\u00e1\b\23\1\2\u00e1\u00e6\3\2\2\2\u00e2\u00e3"+
		"\5\20\t\2\u00e3\u00e4\b\23\1\2\u00e4\u00e6\3\2\2\2\u00e5\u00dc\3\2\2\2"+
		"\u00e5\u00df\3\2\2\2\u00e5\u00e2\3\2\2\2\u00e6%\3\2\2\2\u00e7\u00e8\7"+
		"\33\2\2\u00e8\u00e9\b\24\1\2\u00e9\'\3\2\2\2\u00ea\u00eb\7\34\2\2\u00eb"+
		"\u00f4\b\25\1\2\u00ec\u00ed\5*\26\2\u00ed\u00ee\b\25\1\2\u00ee\u00f4\3"+
		"\2\2\2\u00ef\u00f0\7\36\2\2\u00f0\u00f4\b\25\1\2\u00f1\u00f2\7\35\2\2"+
		"\u00f2\u00f4\b\25\1\2\u00f3\u00ea\3\2\2\2\u00f3\u00ec\3\2\2\2\u00f3\u00ef"+
		"\3\2\2\2\u00f3\u00f1\3\2\2\2\u00f4)\3\2\2\2\u00f5\u00f6\7\22\2\2\u00f6"+
		"\u00fa\b\26\1\2\u00f7\u00f8\7\23\2\2\u00f8\u00fa\b\26\1\2\u00f9\u00f5"+
		"\3\2\2\2\u00f9\u00f7\3\2\2\2\u00fa+\3\2\2\2\24\65>JT\\epv\u0087\u0095"+
		"\u00a2\u00b3\u00bd\u00c8\u00d8\u00e5\u00f3\u00f9";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}