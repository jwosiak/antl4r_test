package com.company;

import com.sun.org.apache.xalan.internal.extensions.ExpressionContext;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;

import java.io.StringReader;
import java.util.LinkedList;

public class Main {

    static RuleExpParser.General_expressionContext createAndEvalExp(String input){
        CharStream in = new UnbufferedCharStream(new StringReader(input));

        RuleExpLexer lexer = new RuleExpLexer(in);

        CommonTokenFactory commonTokenFactory = new CommonTokenFactory(true);
        lexer.setTokenFactory(commonTokenFactory);

        CommonTokenStream tokens = new CommonTokenStream(lexer);
        RuleExpParser parser = new RuleExpParser(tokens);
        return parser.general_expression();
    }


    public static void main(String[] args) throws Exception {

        EnvironmentContext.setFunction("pow", params -> Math.pow((Double) params[0], (Double) params[1]) );
        EnvironmentContext.setVariable("A", new Double(10));

        String[] rules = new String[]{
                "10 * 4 < 100 - 3",
                "13 < 12",
                "3 <= 3 * 4 * 4",
                "10 * 10 * 10 = 1000",
                "10 * 10 * 10 = 1001",
                "1.1 * 2 = 2.2",
                "1.1 * 2 = 2.2 OR 10 < 8",
                "1.1 * 2 = 2.2 AND 10 < 8",
                "(1.1 * 2 = 2.2) OR (10 < 8)",
                "1.1 * 2 = 2.2 OR (10 < 8)",
                "(1.1 * 2) = 2.2 OR 10 < 8",
                "FALSE",
                "TRUE",
                "TRUE OR FALSE",
                "IF TRUE then FALSE",
                "if TRUE then TRUE",
                "if FALSE THEN FALSE",
                "A = 10",
                "pow (4, 2) = 16",
                "IF TRUE AND TRUE THEN true AND true",
                "IF TRUE AND TRUE THEN true AND FALSE",
                "IF TRUE AND false THEN true AND FALSE",
//                "if 4 then true"
        };


        for (String rule : rules){
            RuleExpParser.General_expressionContext cont = createAndEvalExp(rule);
            System.out.println(String.format("%s: %s", rule, cont.value));
        }

    }
}
