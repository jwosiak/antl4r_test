// Generated from /home/Praca/IdeaProjects/antl4r_test/RuleExp.g4 by ANTLR 4.7
package com.company;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link RuleExpParser}.
 */
public interface RuleExpListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#general_expression}.
	 * @param ctx the parse tree
	 */
	void enterGeneral_expression(RuleExpParser.General_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#general_expression}.
	 * @param ctx the parse tree
	 */
	void exitGeneral_expression(RuleExpParser.General_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#variable_declarations_list}.
	 * @param ctx the parse tree
	 */
	void enterVariable_declarations_list(RuleExpParser.Variable_declarations_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#variable_declarations_list}.
	 * @param ctx the parse tree
	 */
	void exitVariable_declarations_list(RuleExpParser.Variable_declarations_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#variable_declaration}.
	 * @param ctx the parse tree
	 */
	void enterVariable_declaration(RuleExpParser.Variable_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#variable_declaration}.
	 * @param ctx the parse tree
	 */
	void exitVariable_declaration(RuleExpParser.Variable_declarationContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#rule_expression}.
	 * @param ctx the parse tree
	 */
	void enterRule_expression(RuleExpParser.Rule_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#rule_expression}.
	 * @param ctx the parse tree
	 */
	void exitRule_expression(RuleExpParser.Rule_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#conditional_rule}.
	 * @param ctx the parse tree
	 */
	void enterConditional_rule(RuleExpParser.Conditional_ruleContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#conditional_rule}.
	 * @param ctx the parse tree
	 */
	void exitConditional_rule(RuleExpParser.Conditional_ruleContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#rule_binary_operator}.
	 * @param ctx the parse tree
	 */
	void enterRule_binary_operator(RuleExpParser.Rule_binary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#rule_binary_operator}.
	 * @param ctx the parse tree
	 */
	void exitRule_binary_operator(RuleExpParser.Rule_binary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#rule_unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterRule_unary_operator(RuleExpParser.Rule_unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#rule_unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitRule_unary_operator(RuleExpParser.Rule_unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#logical_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_expression(RuleExpParser.Logical_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#logical_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_expression(RuleExpParser.Logical_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#logical_binary_operator}.
	 * @param ctx the parse tree
	 */
	void enterLogical_binary_operator(RuleExpParser.Logical_binary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#logical_binary_operator}.
	 * @param ctx the parse tree
	 */
	void exitLogical_binary_operator(RuleExpParser.Logical_binary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#logical_unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterLogical_unary_operator(RuleExpParser.Logical_unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#logical_unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitLogical_unary_operator(RuleExpParser.Logical_unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#arithmetic_expression}.
	 * @param ctx the parse tree
	 */
	void enterArithmetic_expression(RuleExpParser.Arithmetic_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#arithmetic_expression}.
	 * @param ctx the parse tree
	 */
	void exitArithmetic_expression(RuleExpParser.Arithmetic_expressionContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#arithmetic_binary_operator}.
	 * @param ctx the parse tree
	 */
	void enterArithmetic_binary_operator(RuleExpParser.Arithmetic_binary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#arithmetic_binary_operator}.
	 * @param ctx the parse tree
	 */
	void exitArithmetic_binary_operator(RuleExpParser.Arithmetic_binary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#arithmetic_unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterArithmetic_unary_operator(RuleExpParser.Arithmetic_unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#arithmetic_unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitArithmetic_unary_operator(RuleExpParser.Arithmetic_unary_operatorContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#arithmetic_value}.
	 * @param ctx the parse tree
	 */
	void enterArithmetic_value(RuleExpParser.Arithmetic_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#arithmetic_value}.
	 * @param ctx the parse tree
	 */
	void exitArithmetic_value(RuleExpParser.Arithmetic_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#function_invocation}.
	 * @param ctx the parse tree
	 */
	void enterFunction_invocation(RuleExpParser.Function_invocationContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#function_invocation}.
	 * @param ctx the parse tree
	 */
	void exitFunction_invocation(RuleExpParser.Function_invocationContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterParameter_list(RuleExpParser.Parameter_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitParameter_list(RuleExpParser.Parameter_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#empty_parameter_list}.
	 * @param ctx the parse tree
	 */
	void enterEmpty_parameter_list(RuleExpParser.Empty_parameter_listContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#empty_parameter_list}.
	 * @param ctx the parse tree
	 */
	void exitEmpty_parameter_list(RuleExpParser.Empty_parameter_listContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(RuleExpParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(RuleExpParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(RuleExpParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(RuleExpParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#constant_value}.
	 * @param ctx the parse tree
	 */
	void enterConstant_value(RuleExpParser.Constant_valueContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#constant_value}.
	 * @param ctx the parse tree
	 */
	void exitConstant_value(RuleExpParser.Constant_valueContext ctx);
	/**
	 * Enter a parse tree produced by {@link RuleExpParser#bool}.
	 * @param ctx the parse tree
	 */
	void enterBool(RuleExpParser.BoolContext ctx);
	/**
	 * Exit a parse tree produced by {@link RuleExpParser#bool}.
	 * @param ctx the parse tree
	 */
	void exitBool(RuleExpParser.BoolContext ctx);
}