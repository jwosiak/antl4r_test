package com.company;

import java.util.Hashtable;

/**
 * Created by Praca on 12.07.17.
 */
public class EnvironmentContext {
    static Hashtable<String, NArgAction> hashtable = new Hashtable<>();

    static void setVariable(String varName, Comparable val){
        hashtable.put(varName, params -> val);
    }

    static Comparable getVariable(String varName){
        return (Comparable) hashtable.get(varName).doAction();
    }

    static void setFunction(String funName, NArgAction fun){
        hashtable.put(funName, fun);
    }

    static NArgAction getFunction(String funName){
        return hashtable.get(funName);
    }
}
