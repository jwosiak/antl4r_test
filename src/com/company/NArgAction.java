package com.company;

/**
 * Created by Praca on 12.07.17.
 */
public interface NArgAction {
    Comparable doAction (Comparable... args);
}
