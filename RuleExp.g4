grammar RuleExp;


general_expression returns [Boolean value]
    : variable_declarations_list r1 = rule_expression EOF { $value = $r1.value; }
    | r2 = rule_expression EOF { $value = $r2.value; }
    ;

variable_declarations_list
    : variable_declaration DECL_SEP variable_declarations_list
    | variable_declaration DECL_SEP
    ;

variable_declaration
    : i1 = Identifier Assign a = arithmetic_expression { EnvironmentContext.setVariable($i1.text, $a.value); }
    | i2 = Identifier Assign r = rule_expression { EnvironmentContext.setVariable($i2.text, (Comparable) $r.value); }
    ;

rule_expression returns [Boolean value]
    : LP l1 = rule_expression RP
        ( rb = rule_binary_operator rr = rule_expression { $value = (Boolean) $rb.value.doAction($l1.value, $rr.value); }
        |  { $value = $l1.value; }
        )
    | le1 = logical_expression
        ( rb = rule_binary_operator re1 = rule_expression { $value = (Boolean) $rb.value.doAction($le1.value, $re1.value); }
        |  { $value = $le1.value; }
        )
//    : le1 = logical_expression rb = rule_binary_operator re1 = rule_expression
//        { $value = (Boolean) $rb.value.doAction($le1.value, $re1.value); }
//    | LP re2 = rule_expression RP rb = rule_binary_operator re3 = rule_expression
//        { $value = (Boolean) $rb.value.doAction($re2.value, $re3.value); }
//    | LP re4 = rule_expression RP { $value = $re4.value; }
//    | le3 = logical_expression { $value = $le3.value; }
    | cr = conditional_rule { $value = $cr.value; }
    | ru = rule_unary_operator re5 = rule_expression
        { $value = (Boolean) $ru.value.doAction($re5.value); }
    ;

conditional_rule returns [Boolean value]
    : IF cond=rule_expression THEN what_then=rule_expression
        (    ELSE what_else=rule_expression { $value = ($cond.value && $what_then.value) || (!$cond.value && $what_else.value); }
        |    { $value = ($cond.value && $what_then.value) || !$cond.value; }
        )
//    : IF cond=rule_expression THEN what_then=rule_expression ELSE what_else=rule_expression
//        { $value = ($cond.value && $what_then.value) || (!$cond.value && $what_else.value); }
//    | IF cond=rule_expression THEN what_then=rule_expression
//        { $value = ($cond.value && $what_then.value) || !$cond.value; }
    ;

rule_binary_operator returns [NArgAction value]
    : AND { $value = params -> (Boolean) params[0] && (Boolean) params[1]; }
    | OR { $value = params -> (Boolean) params[0] || (Boolean) params[1]; }
    ;

rule_unary_operator returns [NArgAction value]
    : NOT { $value = params -> !((Boolean) params[0]); }
    ;

logical_expression returns [Boolean value]
    : lu = logical_unary_operator a = logical_expression { $value = (Boolean) $lu.value.doAction($a.value); }
    | a1 = arithmetic_expression c = logical_binary_operator a2 = arithmetic_expression
      { $value = (Boolean) $c.value.doAction($a1.value, $a2.value); }
    | b = bool { $value = $b.value; }
    ;

logical_binary_operator returns [NArgAction value]
    : '<' { $value = params -> params[0].compareTo(params[1]) < 0; }
    | '>' { $value = params -> params[0].compareTo(params[1]) > 0; }
    | '=' { $value = params -> params[0].compareTo(params[1]) == 0; }
    | '>=' { $value = params -> params[0].compareTo(params[1]) >= 0; }
    | '<=' { $value = params -> params[0].compareTo(params[1]) <= 0; }
    | '!=' { $value = params -> params[0].compareTo(params[1]) != 0; }
    ;

logical_unary_operator returns [NArgAction value]
    : NOT { $value = params -> !((Boolean) params[0]); }
    ;


arithmetic_expression returns [Comparable value]
    : LP ae1 = arithmetic_expression RP
        (   ab = arithmetic_binary_operator ae2 = arithmetic_expression { $value = (Double) $ab.value.doAction($ae1.value, $ae2.value); }
        |   { $value = $ae1.value; }
        )
//    : LP ae1 = arithmetic_expression RP ( ab = arithmetic_binary_operator ae2 = arithmetic_expression
//        { $value = (Double) $ab.value.doAction($ae1.value, $ae2.value); } )
//    | LP ae = arithmetic_expression RP { $value = $ae.value; }
    | au = arithmetic_unary_operator ae = arithmetic_expression
        { $value = (Double) $au.value.doAction($ae.value); }
    | av = arithmetic_value ab = arithmetic_binary_operator ae = arithmetic_expression
        { $value = (Double) $ab.value.doAction($av.value, $ae.value); }
    | fi = function_invocation { $value = $fi.value; }
    | arith_val = arithmetic_value { $value = $arith_val.value; }
    ;

arithmetic_binary_operator returns [NArgAction value]
    : '+' { $value = (params -> (Double) params[0] + (Double) params[1]); }
    | '-' { $value = (params -> (Double) params[0] - (Double) params[1]); }
    | '*' { $value = (params -> (Double) params[0] * (Double) params[1]); }
    | '/' { $value = (params -> (Double) params[0] / (Double) params[1]); }
    ;

arithmetic_unary_operator returns [NArgAction value]
    : '-' { $value = (params -> ( (-1.0) * (Double) params[0]) ); }
    ;

arithmetic_value returns [Comparable value]
    : c = constant_value { $value = $c.value; }
    | v = variable { $value = $v.value; }
    ;

function_invocation returns [Comparable value]
    : i = Identifier LP p = parameter_list RP
        {
            Comparable[] params = new Comparable[$p.value.size()];
            for (int i = 0; i < $p.value.size(); i++) params[i] = (Comparable) $p.value.get(i);
            $value = EnvironmentContext.getFunction($i.text).doAction(params);
        }
    ;

parameter_list returns [java.util.LinkedList<Comparable> value]
    : p = parameter
        (    SEP pl = parameter_list { $pl.value.add($p.value); $value = $pl.value; }
        |    epl=empty_parameter_list { $epl.value.add($p.value); $value = $epl.value; }   //bo bez ',' na końcu
        )
//    : p = parameter SEP pl = parameter_list { $pl.value.add($p.value); $value = $pl.value; }
//    | p=parameter epl=empty_parameter_list { $epl.value.add($p.value); $value = $epl.value; }
    ;

empty_parameter_list returns [java.util.LinkedList<Comparable> value]
    : { $value = new java.util.LinkedList<Comparable>(); }
    ;

parameter returns [Comparable value]
    : v = variable { $value = $v.value; }
    | ae = arithmetic_expression { $value = $ae.value; }
    | le = logical_expression { $value = (Comparable) $le.value; } //czy typ jest ok do comparable value? chyba tak
    // bez constant_value, bo to się zawiera w arithmetic_expression
    ;

variable returns [Comparable value]
    : i = Identifier { $value = EnvironmentContext.getVariable($i.text); }
    ;

constant_value returns [Comparable value]
    : n = Number {$value = Double.parseDouble($n.text);}
    | b = bool { $value = $b.value; }
    | s = String {$value = $s.text;}
    | d = Date {$value = $d.text;}
    ;

bool returns [Boolean value]
    : TRUE { $value = new Boolean(true); }
    | FALSE { $value = new Boolean(false); }
    ;

Assign : ':=' ;

SEP : ',' ;
DECL_SEP : ';' ;

LP : '(' ;
RP : ')' ;

TRUE : 'TRUE' | 'true' ;
FALSE : 'FALSE' | 'false' ;

IF : 'IF' | 'if' ;
THEN : 'THEN' | 'then' ;
ELSE : 'ELSE' | 'else';
ENDIF : 'ENDIF' | 'endif';

AND :  'AND' | 'and' ;
OR : 'OR' | 'or' ;
NOT: 'NOT' | '!' | 'not' ;

Identifier
    : [a-zA-Z]+[a-zA-Z0-9_-]*
    ;

Number
    : [+-]?[0-9]+('.'[0-9]*)?
    ;

Date
    : [0-9]+
    ;

String
    :QUO (.*?) QUO
    ;

QUO : '"' ;

WS : [ \t\r\n]+ -> skip ; // skip spaces, tabs, newlines